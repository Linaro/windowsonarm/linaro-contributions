#!/usr/bin/env bash

set -euo pipefail

script_dir=$(dirname $(readlink -f $0))
cd $script_dir

die()
{
    echo "$@" >&2
    exit 1
}

[ $# -eq 2 ] || die "usage: repositories_file developers_file"

repositories_file=$1;shift
developers_file=$(readlink -f $1);shift

tmp_dir=$(mktemp -d)
trap "rm -rf $tmp_dir" EXIT

get_url_for_commit()
{
    commit=$1;shift
    url=$1;shift
    url_format=$1;shift

    case $url_format in
        github)
            echo $url/commit/$commit
            ;;
        *)
            die "unknown url format $url_format";;
    esac
}

show_commit()
{
    commit=$1;shift
    url=$1;shift
    url_format=$1;shift

    get_url_for_commit $commit $url $url_format
    git log -n1 -M --numstat $commit
}

list_sha1_per_dev()
{
    while read dev;
    do
        git log --author="$dev" --format=%H
    done < $developers_file
}

list_commits()
{
    url=$1;shift
    url_format=$1;shift

    cd $tmp_dir
    rm -rf repo
    git clone $url repo >& /dev/null

    pushd repo >/dev/null

    commits=$(list_sha1_per_dev | sort -u)

    for commit in $(list_sha1_per_dev | sort -u); do
        show_commit $commit $url $url_format
        echo "---"
    done

    popd >/dev/null
    rm -rf repo
}

while read repo;
do
    url=$(echo $repo | cut -f 1 -d ',')
    url_format=$(echo $repo | cut -f 2 -d ',')
    description=$(echo $repo | cut -f 3 -d ',')

    echo $description - $url
    echo $description - $url | sed -e 's#.#=#g'
    list_commits $url $url_format
done < $repositories_file
